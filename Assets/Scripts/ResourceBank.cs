using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class ResourceBank : MonoBehaviour
{
    private Dictionary<GameResource, int> _resources;
    public Action<GameResource, int> onValueChanged;
    public ResourceBank(GameConfig gameConfig)
    {
        _resources = new Dictionary<GameResource, int>()
        {
            [GameResource.HUMANS] = gameConfig.Humans,
            [GameResource.FOOD] = gameConfig.Food,
            [GameResource.WOOD] = gameConfig.Wood,
            [GameResource.STONE] = gameConfig.Stone,
            [GameResource.GOLD] = gameConfig.Gold,
            [GameResource.HUMANSLVL] = gameConfig.HumansLvl,
            [GameResource.FOODLVL] = gameConfig.FoodLvl,
            [GameResource.WOODLVL] = gameConfig.WoodLvl,
            [GameResource.STONELVL] = gameConfig.StoneLvl,
            [GameResource.GOLDLVL] = gameConfig.GoldLvl
        };
    }


    public void ChangeResource(GameResource r, int v)
    {
        if (!_resources.ContainsKey(r))
            throw new InvalidDataException();


        _resources[r] += v;

        onValueChanged?.Invoke(r, v);

    }

    public int GetResource(GameResource r) => _resources[r];
}