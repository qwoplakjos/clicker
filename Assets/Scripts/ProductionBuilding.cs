using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProductionBuilding : MonoBehaviour
{
    [SerializeField] private Button Button;
    [SerializeField] private GameResource Resource;
    [SerializeField] private GameResource ResourceLvl;
    [SerializeField] private int Amount;
    [SerializeField] private float ProductionTime;
    [SerializeField] private GameObject Slider;
    private Slider slider;



    // Start is called before the first frame update
    void Start()
    {
        slider = Slider.GetComponent<Slider>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Add()
    {
        Game.Instance.ResourceBank.ChangeResource(Resource, Amount);
        StartCoroutine(Wait());
    }

    IEnumerator Wait()
    {
        Button.interactable = false;
        Slider.SetActive(true);
        slider.value = 1;
        float ProdTime = ProductionTime * (1 - (float)Game.Instance.ResourceBank.GetResource(ResourceLvl) / 100);
        float time = ProdTime;

        while (ProdTime > 0)
        {
            ProdTime -= Time.deltaTime;
            slider.value = ProdTime / time;
            yield return null;
        }

        Button.interactable = true;
        Slider.SetActive(false);
    }

}
