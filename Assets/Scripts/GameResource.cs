public enum GameResource
{
    HUMANS,
    FOOD,
    WOOD,
    STONE,
    GOLD,
    HUMANSLVL,
    FOODLVL,
    WOODLVL,
    STONELVL,
    GOLDLVL
}