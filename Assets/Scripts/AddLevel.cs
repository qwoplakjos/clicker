using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddLevel : MonoBehaviour
{
    [SerializeField] private GameResource ResourceLvl;
    [SerializeField] private int Amount;




    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Add()
    {
        Game.Instance.ResourceBank.ChangeResource(ResourceLvl, Amount);
    }

}
