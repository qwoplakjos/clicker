using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ResourceVisual : MonoBehaviour
{
    public GameResource type;
    public Text textField;

    void Awake()
    {
        Game.Instance.ResourceBank.onValueChanged += ChangeValue;
        textField.text = Game.Instance.ResourceBank.GetResource(type).ToString();
    }

    private void ChangeValue(GameResource r, int v)
    {
          textField.text = Game.Instance.ResourceBank.GetResource(type).ToString();
    }
}
