using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    public GameConfig GameConfig;
    public ResourceBank ResourceBank;
    public static Game Instance;

    public void Awake()
    {
        Instance = this;
        ResourceBank = new ResourceBank(GameConfig);
    }
}
