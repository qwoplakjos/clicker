using UnityEngine;

[CreateAssetMenu(menuName = "Config", fileName = "GameConfig")]
public class GameConfig : ScriptableObject
{
    [Range(0, 100)]
    public int Humans;
    [Range(0, 100)]
    public int Food;
    [Range(0, 100)]
    public int Wood;
    [Range(0, 100)]
    public int Stone;
    [Range(0, 100)]
    public int Gold;
    [Range(0, 100)]
    public int HumansLvl;
    [Range(0, 100)]
    public int FoodLvl;
    [Range(0, 100)]
    public int WoodLvl;
    [Range(0, 100)]
    public int StoneLvl;
    [Range(0, 100)]
    public int GoldLvl;

}